# -*- coding: utf-8 -*-
from flask import Flask, jsonify, make_response, request
from flask_httpauth import HTTPBasicAuth
from flask_cors import CORS
from flasgger import Swagger
import cherrypy
from paste.translogger import TransLogger
from datetime import datetime, timedelta
from models import *
from sqlalchemy import and_, func
from cryptography.fernet import Fernet

app = Flask(__name__)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['CORS_HEADERS'] = 'Content-Type'
#_CORS = CORS(app, resources={r"/api/*": {"origins": "http://localhost:8080"}})
_CORS = CORS(app)
AUTH = HTTPBasicAuth()
Swagger(app) #/apidocs/

# ***************************************** Log ***************************************

USERS = {}

def conecta(file):
	key = b'9Et8_1t6qt4m0NLI_-4mS-ycvclWQyveFsZblZFB-Wc='
	cipher_suite = Fernet(key)
	with open(file, 'rb') as file_object:
		for line in file_object:
			encryptedpwd = line
	uncipher_text = (cipher_suite.decrypt(encryptedpwd))
	return bytes(uncipher_text).decode("utf-8") 


app.config['SQLALCHEMY_DATABASE_URI']="mysql+mysqlconnector://" + conecta('myd.bin')

@AUTH.get_password
def get_pw(username):
	""" trae pass """
	USERS['username']=Parametros.query.filter(Parametros.parametro=='sis_api_user').first().valor_char
	USERS['password']=Parametros.query.filter(Parametros.parametro=='sis_api_pass').first().valor_char
	if username in USERS['username']:
		return USERS.get('password')
	return None

# ***************************************** Consultas *********************************

comps_compras_schema = Comp_comprasSchema(many=True)
comp_compras_schema = Comp_comprasSchema()

@app.route('/api/comp_compras', methods=['GET'])
@AUTH.login_required
def get_comp():
	"""
	Esta es una API para extraer Comprobantes de Compras
	Sin parametros trae todos
	---
	tags: 
	- Todos
	responses:
	200:
		description: Json con lista de Comp. de compra
	"""
	comprobantes = Comp_compras.query.all()
	result = comps_compras_schema.dump(comprobantes).data
	return jsonify(result)

@app.route('/api/comp_compras/<int:numero>', methods=['GET'])
@AUTH.login_required
def get_comp_by_number(numero):
	"""
	Esta es una API para extraer Comprobantes de Compras
	Esta trae solo el comprobante con ese numero de Comp.
	---
	tags: 
	- Por numeros
	parameters:
	- name: numero
		in: path
		type: string
		required: true
		description: Numero de comprobante
	responses:
	200:
		description: Json con el Comprobante solicitado

	"""
	comprobante = Comp_compras.query.filter(Comp_compras.numero==numero).first()
	result = comp_compras_schema.dump(comprobante).data
	return jsonify(result)

@app.route('/api/comp_compras/<string:desde>/<string:hasta>', methods=['GET'])
@AUTH.login_required
def get_comp_by_date(desde, hasta):
	"""
	Esta es una API para extraer Comprobantes de Compras
	Esta trae los comprobantes con fecha de carga entre desde y hasta.
	---
	tags: 
	- Entre fechas con fechas de carga
	parameters:
	- name: desde
		in: path
		type: string
		required: true
		description: fecha de carga "Desde" con formato AAAA-MM-DD
	- name: hasta
		in: path
		type: string
		required: true
		description: fecha de carga "Hasta" con formato AAAA-MM-DD
	responses:
	200:
		description: Json con lista de Comp. de compra cargadas entre las fechas indicadas 
	"""
	fechadesde = datetime.strptime(desde, '%Y-%m-%d')
	fechahasta = datetime.strptime(hasta, '%Y-%m-%d')
	dia = timedelta(days=1)
	fechahasta += dia
	comprobantes = Comp_compras.query.filter(Comp_compras.fecha_carga.between(fechadesde,fechahasta)).all()
	result = comps_compras_schema.dump(comprobantes).data
	return jsonify(result)

@app.route('/api/proveedor/<int:id>', methods=['GET'])
@AUTH.login_required
def get_proveedor(id):
	proveedor = Proveedores.query.filter(Proveedores.idproveedor==id).first()
	result = ProveedoresSchema(many=False).dump(proveedor).data
	return jsonify(result)

@app.route('/api/monodrogas', methods=['GET'])
@AUTH.login_required
def get_monodrogas():
	monodrogas = Monodrogas.query.all()
	result = MonodrogasSchema(many=True).dump(monodrogas).data
	return jsonify(result)

@app.route('/api/formas', methods=['GET'])
@AUTH.login_required
def get_formas():
	formas = Tipo_param_detalle.query.filter_by(codigo_param=24).all()
	result = Tipo_param_detalleSchema(many=True).dump(formas).data
	return jsonify(result)

@app.route('/api/unidades', methods=['GET'])
@AUTH.login_required
def get_unidades():
	unidades = Tipo_param_detalle.query.filter_by(codigo_param=25).all()
	result = Tipo_param_detalleSchema(many=True).dump(unidades).data
	return jsonify(result)

@app.route('/api/vias', methods=['GET'])
@AUTH.login_required
def get_vias():
	vias = Tipo_param_detalle.query.filter_by(codigo_param=23).all()
	result = Tipo_param_detalleSchema(many=True).dump(vias).data
	return jsonify(result)

@app.route('/api/frecuencias', methods=['GET'])
@AUTH.login_required
def get_frecuencias():
	frecuencias = San_tipo_frecuencia_toma.query.all()
	result = San_tipo_frecuencia_tomaSchema(many=True).dump(frecuencias).data
	return jsonify(result)

@app.route('/api/productos', methods=['GET'])
@AUTH.login_required
def get_allproductos():
	productos = Productos.query.all()
	result = ProductosSchema(many=True).dump(productos).data
	return jsonify(result)

@app.route('/api/productos/<string:nombre>', methods=['GET'])
@AUTH.login_required
def get_productos(nombre):
	productos = Productos.query.filter(Productos.detalle.like("%"+nombre+"%")).all()
	result = ProductosSchema(many=True).dump(productos).data
	return jsonify(result)

@app.route('/api/productos/<string:prod>/<string:lab>', methods=['GET'])
@AUTH.login_required
def get_productosLab(prod, lab):
	productos = db.session.query(Productos).join(Laboratorios, and_(Productos.idlaboratorio==Laboratorios.idlaboratorio,Laboratorios.nombre==lab)).filter(Productos.detalle.like("%"+prod+"%")).all()
	result = ProductosSchema(many=True).dump(productos).data
	return jsonify(result)

@app.route('/api/genericos/<int:idmono>/<int:idforma>', methods=['GET'])
@AUTH.login_required
def get_genericos(idmono,idforma):
	genericos = Genericos.query.filter(and_(Genericos.idmonodroga==idmono,Genericos.idforma==idforma,Genericos.productos!=None)).all()
	result = GenericosSchema(many=True).dump(genericos).data
	return jsonify(result)

@app.route('/api/genericos/<int:idmono>', methods=['GET'])
@AUTH.login_required
def get_genericos_mono(idmono):
	genericos = Genericos.query.filter(and_(Genericos.idmonodroga==idmono,Genericos.productos!=None)).all()
	result = GenericosSchema(many=True).dump(genericos).data
	return jsonify(result)

#incluido aca para no perder la forma en que se hace un POST
@app.route("/api/laboratorio", methods=["POST"])
def add_lab():
	nombre = request.json['nombre']
	
	new_lab = Laboratorios(nombre)

	db.session.add(new_lab)
	db.session.commit()
	return jsonify(new_lab)

@app.errorhandler(404)
def not_found():
	""" x erro """
	return make_response(jsonify({'error': 'Not found'}), 404)

def run_server():
	
	# Enable WSGI access logging via Paste
	app_logged = TransLogger(app)

	# Mount the WSGI callable object (app) on the root directory
	cherrypy.tree.graft(app_logged, '/')

	# Set the configuration of the web server
	cherrypy.config.update(
		{'engine.autoreload_on': False,
		'log.screen': False,
		'server.socket_port': 5000,
		'server.socket_host': '0.0.0.0'})

	# Start the CherryPy WSGI web server
	cherrypy.engine.start()
	cherrypy.engine.block()


if __name__ == '__main__':
	db.init_app(app)
	ma.init_app(app)
	print('Iniciado el Server...')
	#app.run(debug=True)
	run_server()
