from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from sqlalchemy import and_, func
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method

db = SQLAlchemy()
ma = Marshmallow()

# *********************************** tablas->ORM ***************************************

genericos_productos = db.Table('genericos_productos',
    db.Column('idgenerico',db.Integer, db.ForeignKey('genericos.idgenerico'), primary_key=True),
    db.Column('idproducto',db.Integer, db.ForeignKey('productos_stock.idproducto'), primary_key=True)
    )

class Comp_compras(db.Model):
    idcomprob = db.Column(db.String(28), primary_key=True)
    numero = db.Column(db.Integer)
    idproveedor = db.Column(db.Integer, db.ForeignKey('proveedores.idproveedor'))
    tipocomp = db.Column(db.String(12))
    prefijo = db.Column(db.String(1))
    pto_venta = db.Column('idsucursal',db.Integer)
    numero = db.Column(db.Integer)
    iddeposito = db.Column(db.Integer, index=True)
    fecha_cont = db.Column(db.Date)
    fecha = db.Column(db.Date, index=True)
    fecha_venc = db.Column(db.Date)
    fecha_rece = db.Column(db.Date)
    subtotal = db.Column(db.Float)
    descuento = db.Column(db.Float)
    imponible = db.Column(db.Float)
    importe = db.Column(db.Float)
    importe_orig = db.Column(db.Float)
    saldo = db.Column(db.Float)
    moneda = db.Column(db.String)
    cotizacion = db.Column(db.Float)
    estado = db.Column(db.String(12))
    operador = db.Column(db.String(12))
    libre = db.Column(db.Text)
    fecha_carga = db.Column('fechahora',db.DateTime)
    idtalonario = db.Column(db.Integer)
    idcondpago = db.Column(db.Integer, index=True)
    idasiento = db.Column(db.String(12))
    cae = db.Column('cai',db.String(14))
    cae_vto = db.Column('cai_vto',db.Date)
    idlaboratorio = db.Column(db.Integer)
    pago_a_laboratorio = db.Column(db.Integer)
    yo = db.Column(db.Integer)
    pago_a_otros = db.Column(db.Integer)
    descripcion_otros = db.Column(db.String(100))
    btransferencia = db.Column(db.Integer)
    fecha_transferencia = db.Column(db.Date)
    idcuenta = db.Column(db.String(8))
    proveedor = db.relationship('Proveedores', backref='proveedor')    
    detalle = db.relationship('Comp_compras_detalle', backref='comp_compras',lazy='dynamic')    
    alicuotas = db.relationship('Comp_compras_alicuotas', backref='comp_compras',lazy='dynamic') 
    
class Proveedores(db.Model):
    idproveedor = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(80))
    cuit = db.Column(db.Integer)
    idpostal = db.Column(db.String(10), db.ForeignKey('codigos_postales.idpostal'))
    cond_iva = db.Column(db.String(2), db.ForeignKey('categoriasiva.idiva'))
    iva = db.relationship('Categorias_iva', backref='iva')  
    localidad = db.relationship('Codigos_postales', backref='proveedor')  
    
class Laboratorios(db.Model):
    idlaboratorio = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(80))
    def __init__(self,  nombre):
        self.nombre = nombre

class Codigos_postales(db.Model):
    idpostal = db.Column(db.String(8), primary_key=True)
    localidad = db.Column(db.String(40))
    provincia = db.Column(db.String(40))

class Productos(db.Model):
    idproducto = db.Column(db.Integer, primary_key=True)
    detalle = db.Column(db.String(80))
    laboratorio = db.relationship('Laboratorios', backref='laboratorio')
    idlaboratorio = db.Column(db.Integer, db.ForeignKey('laboratorios.idlaboratorio'))
    
class Comp_compras_detalle(db.Model):
    idcomprob = db.Column(db.String(28), db.ForeignKey('comp_compras.idcomprob'), primary_key=True)
    nro_linea = db.Column(db.Integer, primary_key=True)
    idproducto = db.Column(db.Integer, db.ForeignKey('productos.idproducto'), index=True)
    descripcion = db.Column(db.String(80))
    precio = db.Column(db.Float)
    descuento1 = db.Column(db.Float)
    descuento2 = db.Column(db.Float)
    descuento3 = db.Column(db.Float)
    precio_dto = db.Column(db.Float)
    cantidad = db.Column(db.Float)
    importe = db.Column(db.Float)
    porc_impint = db.Column(db.Float)
    impinterno = db.Column(db.Float)
    porc_iva = db.Column(db.Float)
    producto = db.relationship('Productos', backref='producto')

class Comp_compras_alicuotas(db.Model):
    idcomprob = db.Column(db.String(28), db.ForeignKey('comp_compras.idcomprob'), primary_key=True)
    idalicuota = db.Column(db.String(2), db.ForeignKey('alicuotasiva.cod_alicuo'), primary_key=True)
    gravado = db.Column(db.Float)
    porcentaje = db.Column(db.Float)
    importe = db.Column(db.Float)
    alicuota = db.relationship('Alicuotasiva', backref='tasa')

class Alicuotasiva(db.Model):
    cod_alicuo = db.Column(db.String(2), primary_key=True)
    desc_alicu = db.Column(db.String(35))

class Parametros(db.Model):
    __tablename__="Parametros_detalle"
    parametro = db.Column(db.String(20), primary_key=True)
    valor_char = db.Column(db.String(100))

class Categorias_iva(db.Model):
    __tablename__="categoriasiva"
    idiva = db.Column(db.String(2),primary_key=True)
    desc_iva = db.Column(db.String(30))

class Monodrogas(db.Model):
    idmonodroga = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(60))

class Tipo_param_detalle(db.Model):
    codigo_param = db.Column(db.Integer, primary_key=True)
    codigo = db.Column(db.Integer, primary_key=True)
    codigoc = db.Column(db.Integer, primary_key=True)
    descripcion_param = db.Column(db.String(120))

class San_tipo_frecuencia_toma(db.Model):
    idfrecuencia = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(50))
    cant_total = db.Column(db.Integer)

class Productos_stock(db.Model):
    idproducto = db.Column(db.Integer, primary_key=True)
    cantidad = db.Column(db.Integer)
    idlote = db.Column(db.String(28), primary_key=True) 
    genricos = db.relationship('Genericos', secondary=genericos_productos, backref=db.backref('albums_to_artists_table_backref', lazy='dynamic'))   
    
class Genericos(db.Model):
    idgenerico = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(70))
    idtipogenerico = db.Column(db.Integer)
    idmonodroga = db.Column(db.Integer)
    idforma = db.Column(db.Integer)
    productos = db.relationship('Productos_stock', secondary=genericos_productos, backref='genericos',lazy='dynamic')   

    @hybrid_property
    def suma(self):
        return sum(p.cantidad for p in self.productos)

    @suma.expression
    def suma(cls):
        return func.sum(Productos_stock.cantidad)
 
# *********************************** ORM -> Json ***************************************

class CategoriasIvaSchema(ma.ModelSchema):
    class Meta:
        model = Categorias_iva
        fields = ('idiva','desc_iva')

class CodigosPostalesSchema(ma.ModelSchema):
    class Meta:
        model = Codigos_postales
        fields = ('idpostal','localidad','provincia')

class ProveedoresSchema(ma.ModelSchema):
    class Meta:
        model = Proveedores
        fields = ('idproveedor','nombre','cuit','iva','localidad')
    iva = ma.Nested(CategoriasIvaSchema, many=False)
    localidad = ma.Nested(CodigosPostalesSchema, many=False)

class LaboratoriosSchema(ma.ModelSchema):
    class Meta:
        model = Laboratorios
        fields = ('idlaboratorio','nombre')

class Productos_stockSchema(ma.ModelSchema):
    class Meta:
        model = Productos_stock
        fields = ('idproducto','idlote','cantidad')

class ProductosSchema(ma.ModelSchema):
    laboratorio = ma.Nested(LaboratoriosSchema, many=False)
    stock = ma.Nested(Productos_stockSchema, many=True)
    class Meta:
        model = Productos
        fields = ('idproducto','detalle','laboratorio','stock')

class AlicuotasivaSchema(ma.ModelSchema):
    class Meta:
        model = Alicuotasiva
        fields = ('cod_alicuo','desc_alicu')

class Comp_compras_detalleSchema(ma.ModelSchema):
    producto = ma.Nested(ProductosSchema, many=False)
    class Meta:
        model = Comp_compras_detalle
        fields = (
            'nro_linea', 'descuento1','descuento2', 'descuento3', 
            'precio','precio_dto','cantidad','importe', 'porc_iva','producto'
        )

class Comp_compras_alicuotasSchema(ma.ModelSchema):
    alicuota = ma.Nested(AlicuotasivaSchema, many=False)
    class Meta:
        model = Comp_compras_alicuotas
        fields = ('alicuota', 'gravado','importe','porcentaje')

class Comp_comprasSchema(ma.ModelSchema):
    detalle = ma.Nested(Comp_compras_detalleSchema, many=True)
    alicuotas = ma.Nested(Comp_compras_alicuotasSchema, many=True)
    proveedor = ma.Nested(ProveedoresSchema, many=False)
    class Meta:
        model = Comp_compras
        fields = (
            'idcomprob','proveedor','tipocomp','prefijo','pto_venta',
            'numero','fecha','subtotal','importe','cae','cae_vto','detalle',
            'alicuotas','fecha_carga'
        )

class MonodrogasSchema(ma.ModelSchema):
    class Meta:
        model = Monodrogas
        fields = ('idmonodroga','descripcion')

class Tipo_param_detalleSchema(ma.ModelSchema):
    class Meta:
        model = Tipo_param_detalle
        fields = ('codigo_param','codigo','codigoc','descripcion_param')

class San_tipo_frecuencia_tomaSchema(ma.ModelSchema):
    class Meta:
        model = San_tipo_frecuencia_toma
        fields = ('idfrecuencia','descripcion','cant_total')

class Productos_stockSchema(ma.ModelSchema):
    class Meta:
        model = Productos_stock
        fields = ('idproducto','idlote','cantidad')

class GenericosSchema(ma.ModelSchema):
    productos = ma.Nested(Productos_stockSchema, many=True)
    class Meta:
        model = Genericos
        fields = ('idgenerico','descripcion','idtipogenerico','idmonodroga','idforma','productos','suma')        